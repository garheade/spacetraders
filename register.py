import tkinter as tk
from tkinter import messagebox
import requests
import json
import sqlite3
import yaml

# Load configuration from config.yml
with open('config.yml') as f:
    config = yaml.safe_load(f)

# Create database connection
conn = sqlite3.connect(config['database'])

# Check if user_info table exists
cursor = conn.cursor()
cursor.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='user_info'")
table_exists = cursor.fetchone()

# Create user_info table if it doesn't exist
if not table_exists:
    conn.execute('''
        CREATE TABLE user_info (
            username TEXT,
            email TEXT,
            faction TEXT
        )
    ''')

    # Prompt user to confirm overwrite if there is existing data
    if cursor.execute("SELECT COUNT(*) FROM user_info").fetchone()[0] > 0:
        if messagebox.askyesno("Overwrite Data", "User info data already exists. Do you want to overwrite it?"):
            conn.execute("DELETE FROM user_info")
        else:
            messagebox.showinfo("Data Not Overwritten", "User info data was not overwritten.")
            conn.close()
            exit()

# Get faction symbols from faction_basics table
faction_symbols = [row[0] for row in conn.execute("SELECT symbol FROM faction_basics")]

# Function to handle form submission
def submit_form():
    username = username_entry.get()
    email = email_entry.get()
    faction = faction_var.get()

    # Check if all fields are filled
    if not username or not email or not faction:
        messagebox.showerror("Error", "Please fill in all fields.")
        return

    # Create payload for API request
    payload = {
        "faction": faction,
        "symbol": username,
        "email": email
    }

    # Send POST request
    response = requests.post(config['api_url'] + 'register', headers=config['headers'], json=payload)
    data = response.json()

    # Insert data into user_info table
    conn.execute('''
        INSERT INTO user_info (username, email, faction)
        VALUES (?, ?, ?)
    ''', (username, email, faction))

    # Update config.yml with response data
    config['username'] = username
    config['email'] = email
    config['faction'] = data['data']['faction']['symbol']
    config['token'] = data['data']['token']

    # Write updated config to config.yml file
    with open('config.yml', 'w') as f:
        yaml.dump(config, f)

    # Display success message
    messagebox.showinfo("Registration Successful", "Registration completed successfully.")

    # Close database connection
    conn.commit()
    conn.close()

    # Exit the application
    root.destroy()

# Create Tkinter window
root = tk.Tk()
root.title("User Registration")

# Create username label and entry
username_label = tk.Label(root, text="Username:")
username_label.pack()
username_entry = tk.Entry(root)
username_entry.pack()

# Create email label and entry
email_label = tk.Label(root, text="Email:")
email_label.pack()
email_entry = tk.Entry(root)
email_entry.pack()

# Create faction label and dropdown
faction_label = tk.Label(root, text="Faction:")
faction_label.pack()
faction_var = tk.StringVar(root)
faction_dropdown = tk.OptionMenu(root, faction_var, *faction_symbols)
faction_dropdown.pack()

# Create submit button
submit_button = tk.Button(root, text="Submit", command=submit_form)
submit_button.pack()

# Run the Tkinter event loop
root.mainloop()

