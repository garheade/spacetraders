#!/bin/bash -eu

## Check if local db is available. Create if not.
if [ ! -e sttest.db ]; then
  echo "No Database Ready.";
  read -n 1 -p "Create Database? [y,N]" createdb
  case $createdb in
    y|Y ) echo "creating (placeholder for test)"
    ;;
    n|N ) echo "Exiting: No Database available."
    ;;
  esac;
else
  echo "Database Ready. Checking Server status."
fi

curl 'https://api.spacetraders.io/v2/' > serverstatus.json


