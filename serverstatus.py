#!/usr/bin/env python

import os
import json
import urllib.request
import subprocess
import yaml
import sqlite3
from datetime import datetime

# Load configuration variables from file
with open('config.yml') as f:
    config = yaml.safe_load(f)

# Check if SQLite is installed
def check_sqlite_installed():
    try:
        subprocess.check_output(['sqlite3', '--version'])
    except subprocess.CalledProcessError:
        print('SQLite is not installed. Please install SQLite and try again.')
        exit()

# Check if local db is available. Create if not.
def check_create_db():
    if not os.path.exists(config['database']):
        print('No Database Ready.')
        createdb = input('Create Database? [y,N] ')
        if createdb.lower() == 'y':
            print('Creating database...')
            subprocess.run(['sqlite3', config['database'], ''])
        else:
            print('Exiting: No Database available.')
            exit()

    else:
        print('Database Ready. Checking Server status.')

# Fetch the server status and save to file
def fetch_server_status():
    with urllib.request.urlopen(config['api_url']) as url:
        data = json.loads(url.read().decode())

        # Add last updated time to data
        data['last_updated'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        # Save data to file
        with open(config['server_status_file'], 'w') as outfile:
            json.dump(data, outfile)

        # Import data to SQLite table
        conn = sqlite3.connect(config['database'])
        c = conn.cursor()

        # Create table if it doesn't exist
        c.execute('''CREATE TABLE IF NOT EXISTS server_status
                     (status text, agent_count int, ship_count int, system_count int, waypoint_count int, last_updated text)''')

        # Insert data into table
        c.execute("INSERT INTO server_status (status, agent_count, ship_count, system_count, waypoint_count, last_updated) VALUES (?, ?, ?, ?, ?, ?)",
                  (data['status'], data['stats']['agents'], data['stats']['ships'], data['stats']['systems'], data['stats']['waypoints'], data['last_updated']))
        conn.commit()
        conn.close()

# Main function that calls all other functions
def main():
    check_sqlite_installed()
    check_create_db()
    fetch_server_status()

if __name__ == '__main__':
    main()
