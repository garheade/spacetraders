import json
import sqlite3
import urllib.request
import yaml

# Load configuration from config.yml
with open('config.yml') as f:
    config = yaml.safe_load(f)

# Create database connection
conn = sqlite3.connect(config['database'])

# Create faction_basics table if it doesn't exist
conn.execute('''
    CREATE TABLE IF NOT EXISTS faction_basics (
        symbol TEXT PRIMARY KEY,
        name TEXT,
        description TEXT,
        headquarters TEXT
    )
''')

# Fetch existing factions from faction_basics table
existing_factions = set()
for row in conn.execute("SELECT symbol FROM faction_basics"):
    existing_factions.add(row[0])

# Fetch faction data and insert into faction_basics table
api_url = config['api_url'] + '/factions'
with urllib.request.urlopen(api_url) as url:
    data = json.loads(url.read().decode())
    factions = data['data']
    for faction in factions:
        symbol = faction['symbol']
        name = faction['name']
        description = faction['description']
        headquarters = faction['headquarters']

        # Check if faction already exists
        if symbol in existing_factions:
            # Update existing faction
            conn.execute('''
                UPDATE faction_basics
                SET name = ?,
                    description = ?,
                    headquarters = ?
                WHERE symbol = ?
            ''', (name, description, headquarters, symbol))
        else:
            # Insert new faction
            conn.execute('''
                INSERT INTO faction_basics (
                    symbol,
                    name,
                    description,
                    headquarters
                ) VALUES (?, ?, ?, ?)
            ''', (symbol, name, description, headquarters))

# Commit changes and close database connection
conn.commit()
conn.close()

