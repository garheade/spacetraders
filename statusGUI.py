#!/usr/bin/env python

import os
import json
import urllib.request
import subprocess
import yaml
import sqlite3
from datetime import datetime
import tkinter as tk

# Load configuration variables from file
with open('config.yml') as f:
    config = yaml.safe_load(f)

# Fetch server status and save to database
subprocess.run(['python', 'serverstatus.py'])

# Connect to SQLite database and fetch latest status
conn = sqlite3.connect(config['database'])
c = conn.cursor()
c.execute('SELECT * FROM server_status ORDER BY last_updated DESC LIMIT 1')
row = c.fetchone()
conn.close()

# Create GUI window and display latest status
root = tk.Tk()
root.title('SpaceTraders Server Status')

# Create labels for displaying server status
status_label = tk.Label(root, text='Status: ')
status_value_label = tk.Label(root, text=row[0])
agent_count_label = tk.Label(root, text='Agent Count: ')
agent_count_value_label = tk.Label(root, text=row[1])
ship_count_label = tk.Label(root, text='Ship Count: ')
ship_count_value_label = tk.Label(root, text=row[2])
system_count_label = tk.Label(root, text='System Count: ')
system_count_value_label = tk.Label(root, text=row[3])
waypoint_count_label = tk.Label(root, text='Waypoint Count: ')
waypoint_count_value_label = tk.Label(root, text=row[4])
last_updated_label = tk.Label(root, text='Last Updated: ')
last_updated_value_label = tk.Label(root, text=row[5])

# Position labels on grid
status_label.grid(row=0, column=0, sticky='w')
status_value_label.grid(row=0, column=1, sticky='w')
agent_count_label.grid(row=1, column=0, sticky='w')
agent_count_value_label.grid(row=1, column=1, sticky='w')
ship_count_label.grid(row=2, column=0, sticky='w')
ship_count_value_label.grid(row=2, column=1, sticky='w')
system_count_label.grid(row=3, column=0, sticky='w')
system_count_value_label.grid(row=3, column=1, sticky='w')
waypoint_count_label.grid(row=4, column=0, sticky='w')
waypoint_count_value_label.grid(row=4, column=1, sticky='w')
last_updated_label.grid(row=5, column=0, sticky='w')
last_updated_value_label.grid(row=5, column=1, sticky='w')

# Create refresh and exit buttons
def refresh_status():
    # Fetch server status and update labels
    subprocess.run(['python', 'serverstatus.py'])

    conn = sqlite3.connect(config['database'])
    c = conn.cursor()
    c.execute('SELECT * FROM server_status ORDER BY last_updated DESC LIMIT 1')
    row = c.fetchone()
    conn.close()

    status_value_label.config(text=row[0])
    agent_count_value_label.config(text=row[1])
    ship_count_value_label.config(text=row[2])
    system_count_value_label.config(text=row[3])
    waypoint_count_value_label.config(text=row[4])
    last_updated_value_label.config(text=row[5])

refresh_button = tk.Button(root, text='Refresh', command=refresh_status)
refresh_button.grid(row=6, column=1, sticky='e')
exit_button = tk.Button(root, text='Exit', command=root.quit)
exit_button.grid(row=6, column=2, sticky='e')

root.mainloop()
